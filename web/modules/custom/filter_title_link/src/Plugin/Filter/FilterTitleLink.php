<?php
/**
 * @file
 * Contains Drupal\filter_title_link\Plugin\Filter\FilterCelebrate
 */

namespace Drupal\filter_title_link\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to help celebrate good times!
 *
 * @Filter(
 *   id = "filter_title_link",
 *   title = @Translation("Title link filter"),
 *   description = @Translation("Replaces node title in brackets by link of this node."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterTitleLink extends FilterBase {
  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    return new FilterProcessResult(_filter_title_link_replace($text, $this));
  }
}
