<?php

namespace Drupal\front_redirect\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirect front page.
 */
class FrontRedirectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return([
      KernelEvents::REQUEST => [
        ['redirectFrontPage'],
      ],
    ]);
  }

  /**
   * Redirect requests for front page to /msk.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Event object.
   */
  public function redirectFrontPage(GetResponseEvent $event) {
    $request = $event->getRequest();

    // Only redirect the front page.
    if ($request->getRequestUri() !== '/') {
      return;
    }

    // $redirect_url = Url::fromUri('entity:node/55');
    // $response = new RedirectResponse($redirect_url->toString(), 301);.
    $response = new RedirectResponse('/msk', 301);
    $event->setResponse($response);
  }

}
