<?php

/**
 * @file
 * Contains \Drupal\prevnext_station\Plugin\Block\PrevNextStationBlock.
 */

namespace Drupal\prevnext_station\Plugin\Block;

use Drupal\Core\Link;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;


/**
 * Provides a block with previous and next stations links.
 *
 * @Block(
 *   id = "prevnext_station_block",
 *   admin_label = @Translation("Previous Next Station Block"),
 *   category = @Translation("Blocks")
 * )
 */
class PrevNextStationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      $line = $node->field_line->getString();
      $order = $node->field_line_order->getString();

      $link_prev = [];
      $prev = $this->generateNextPrevious($line, $order - 1);
      if ($prev) {
        $link_prev = [
          '#prefix' => '<div class="nav-prev">',
          '#suffix' => '</div>',
          '#markup' => $prev->toString(),
        ];
      }
      $link_next = [];
      $next = $this->generateNextPrevious($line, $order + 1);
      if ($next) {
        $link_next = [
          '#prefix' => '<div class="nav-next">',
          '#suffix' => '</div>',
          '#markup' => $next->toString(),
        ];
      }
      $build['link'] = [
        '#markup' => render($link_prev) . render($link_next),
      ];
    }

    return $build;
  }

  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    //if you depends on \Drupal::routeMatch()
    //you must set context of this block with 'route' context tag.
    //Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

  /**
   * Lookup the next or previous node
   */
  private function generateNextPrevious($line, $order) {

    $query = \Drupal::entityQuery('node');
    $nid = $query->condition('type', 'station')
      ->condition('field_line', $line)
      ->condition('field_line_order', $order)
      ->range(0, 1)
      ->execute();

    if (!empty($nid) && is_array($nid)) {
      $nid = array_values($nid);
      $nid = $nid[0];
      $node = \Drupal::service('entity_type.manager')->getStorage('node')->load($nid);
      $title = $node->getTitle();
      $url = Url::fromRoute('entity.node.canonical', ['node' => $nid]);
      return Link::fromTextAndUrl($title, $url);
    }
    else {
      return;
    }
  }
}
